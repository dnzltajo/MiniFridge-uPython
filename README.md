# Programmable Mini Fridge 

ECE 505/513 - Undergraduate Thesis 

    A mini fridge using peltier module that utilizes on-off feedback control

<p align="center">
  <img width="600" height="375" src="https://gitlab.com/dnzltajo/MiniFridge-uPython/-/raw/master/images/Picture1.png">
</p>

## Function

<p align="center">
  <img width="500" height="350" src="https://gitlab.com/dnzltajo/MiniFridge-uPython/-/raw/master/images/fun.jpg">
</p>  

- Inputs

  - Temperature Sensor
 
  - Rotary Encoder 
 
  - Push Buttons
 
  - RTC
- Outputs
 
  - LCD
 
  - Relay 

## Libraries 

* [DS1307 RTC](https://github.com/mcauser/micropython-tinyrtc-i2c)

* [LCD i2c](https://github.com/dhylands/python_lcd)

* [Rotary Encoder](https://github.com/miketeachman/micropython-rotary)






